jQueryProjSE
======
jQuery Project - Simple Example 
jQuery 與 jQuery UI 相關代碼收錄

Feature
------
+ Ajax
    + [Ajax 跨域訪問](https://gitlab.com/Enoxs/Document/blob/master/Web/Ajax/Ajax-%E8%B7%A8%E5%9F%9F%E8%A8%AA%E5%95%8F.md)
+ Alert
+ Basis
+ Copy
+ [DataTable](https://rhzenoxs.github.io/jQueryProjSE/src/DataTable/DataTable.html)
+ Dialog
+ [I-Checks](https://rhzenoxs.github.io/jQueryProjSE/src/I-Checks/iCheck.html)
+ [Keyboard](https://rhzenoxs.github.io/jQueryProjSE/src/Keyboard/KeyBoard.html)
+ [Markdown](https://rhzenoxs.github.io/jQueryProjSE/src/Markdown/Markdown.html)
+ [Moment](https://rhzenoxs.github.io/jQueryProjSE/src/Moment/Moment.html)
+ [Qunit](https://rhzenoxs.github.io/jQueryProjSE/src/Qunit/QUnit.html)
+ Script
+ SideMent
+ UI


